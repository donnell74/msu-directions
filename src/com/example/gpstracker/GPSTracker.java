package com.example.gpstracker;

import java.util.ArrayList;
import java.util.Timer;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class GPSTracker extends Activity implements SensorEventListener {
	
	Compass myCompass;
	
	double bearing;
	
	SensorManager sensorManager;
	private Sensor sensorAccelerometer;
	private Sensor sensorMagneticField;

	private float[] valuesAccelerometer;
	private float[] valuesMagneticField;

	private float[] matrixR;
	private float[] matrixI;
	private float[] matrixValues;
	
	GeomagneticField geoField;
	
	Handler mHandler;
	
	boolean startStop;
	
	ValueAnimator anim;
	
	Timer mTimer;
	
	Intent intent;
	int pos;
	
	SharedPreferences prefs;
	
	ArrayList<Location> locs;
	ArrayList<String> locNames;
	ArrayList<String> locLat;
	ArrayList<String> locLong;
	
	Location blankLoc = new Location("Google");
	Location currentLocation;
	
	boolean noNorth;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gpstracker);
		
		bearing = 0;
		
		myCompass = (Compass) findViewById(R.id.myCompass);
		
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensorAccelerometer = sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorMagneticField = sensorManager
				.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		valuesAccelerometer = new float[3];
		valuesMagneticField = new float[3];

		matrixR = new float[9];
		matrixI = new float[9];
		matrixValues = new float[3];
		
		mHandler = new Handler();
		
		startStop = false;
		
		noNorth = true;
		
		anim = ValueAnimator.ofFloat(0f, 1f);
		
		prefs = this.getSharedPreferences(
			      "com.example.gpstracker.locations", Context.MODE_PRIVATE);
		pos = prefs.getInt("pos", 9999);
		
		locs = new ArrayList<Location>(100);
		locNames = new ArrayList<String>(100);
		locLat = new ArrayList<String>(100);
		locLong = new ArrayList<String>(100);
		
		// initialize location objects
		
		Location cheekHall = blankLoc;
		cheekHall.setLatitude(37.1990232);
		locLat.add("37.1990232");
		cheekHall.setLongitude(-93.2768292);
		locLong.add("-93.2768292");
		locs.add(cheekHall);
		locNames.add("Cheek Hall");
		
		Location siceluffHall = blankLoc;
		siceluffHall.setLatitude(37.1965001);
		locLat.add("37.1965001");
		siceluffHall.setLongitude(-93.2751146);
		locLong.add("-93.2751146");
		locs.add(siceluffHall);
		locNames.add("Siceluff Hall");
		
		Location carringtonHall = blankLoc;
		carringtonHall.setLatitude(37.19867);
		locLat.add("37.19867");
		carringtonHall.setLongitude(-93.2782363);
		locLong.add("-93.2782363");
		locs.add(carringtonHall);
		locNames.add("Carrington Hall");
		
		Location strong = blankLoc;
		strong.setLatitude(37.1986407);
		locLat.add("37.1986407");
		strong.setLongitude(-93.2839711);
		locLong.add("-93.2839711");
		locs.add(strong);
		locNames.add("Strong Hall");
		
		Location glass = blankLoc;
		glass.setLatitude(37.1987932);
		locLat.add("37.1987932");
		glass.setLongitude(-93.2829054);
		locLong.add("-93.2829054");
		locs.add(glass);
		locNames.add("Glass Hall");
		
		Location kemper = blankLoc;
		kemper.setLatitude(37.1974011);
		locLat.add("37.1974011");
		kemper.setLongitude(-93.2826526);
		locLong.add("-93.2826526");
		locs.add(kemper);
		locNames.add("Kemper Hall");
		
		Location temple = blankLoc;
		temple.setLatitude(37.1981941);
		locLat.add("37.1981941");
		temple.setLongitude(-93.2825282);
		locLong.add("-93.2825282");
		locs.add(temple);
		locNames.add("Temple Hall");
		
		Location library = blankLoc;
		library.setLatitude(37.1985353);
		locLat.add("37.1985353");
		library.setLongitude(-93.2817594);
		locLong.add("-93.2817594");
		locs.add(library);
		locNames.add("Library");
		
		Location mcdonaldarena = blankLoc;
		mcdonaldarena.setLatitude(37.1991398);
		locLat.add("37.1991398");
		mcdonaldarena.setLongitude(-93.2804953);
		locLong.add("-93.2804953");
		locs.add(mcdonaldarena);
		locNames.add("Mcdonald Arena");
		
		Location psu = blankLoc;
		psu.setLatitude(37.1993223);
		locLat.add("37.1993223");
		psu.setLongitude(-93.2785994);
		locLong.add("-93.2785994");
		locs.add(psu);
		locNames.add("Plaster Student Union");
		
		Location bookstore = blankLoc;
		bookstore.setLatitude(37.2004134);
		locLat.add("37.2004134");
		bookstore.setLongitude(-93.2785002);
		locLong.add("-93.2785002");
		locs.add(bookstore);
		locNames.add("MSU Bookstore");
		
		Location taylerhealth = blankLoc;
		taylerhealth.setLatitude(37.2013787);
		locLat.add("37.2013787");
		taylerhealth.setLongitude(-93.2777697);
		locLong.add("-93.2777697");
		locs.add(taylerhealth);
		locNames.add("Tayler Health & Wellness");
		
		Location universityHall = blankLoc;
		universityHall.setLatitude(37.2015259);
		locLat.add("37.2015259");
		universityHall.setLongitude(-93.2772586);
		locLong.add("-93.2772586");
		locs.add(universityHall);
		locNames.add("University Hall");
		
		Location madisonHall = blankLoc;
		madisonHall.setLatitude(37.2015698);
		locLat.add("37.2015698");
		madisonHall.setLongitude(-93.277637);
		locLong.add("-93.277637");
		locs.add(madisonHall);
		locNames.add("Madison Hall");
		
		Location royellis = blankLoc;
		royellis.setLatitude(37.198444);
		locLat.add("37.198444");
		royellis.setLongitude(-93.2770464);
		locLong.add("-93.2770464");
		locs.add(royellis);
		locNames.add("Roy Ellis Hall");
		
		Location hill = blankLoc;
		hill.setLatitude(37.1978269);
		locLat.add("37.1978269");
		hill.setLongitude(-93.2777999);
		locLong.add("-93.2777999");
		locs.add(hill);
		locNames.add("Hill Hall");
		
		Location pummel = blankLoc;
		pummel.setLatitude(37.1985262);
		locLat.add("37.1985262");
		pummel.setLongitude(-93.278239);
		locLong.add("-93.278239");
		locs.add(pummel);
		locNames.add("Pummill Hall");
		
		Location karls = blankLoc;
		karls.setLatitude(37.1980006);
		locLat.add("37.1980006");
		karls.setLongitude(-93.277997);
		locLong.add("-93.277997");
		locs.add(karls);
		locNames.add("Karls Hall");
		
		Location craig = blankLoc;
		craig.setLatitude(37.1978267);
		locLat.add("37.1978267");
		craig.setLongitude(-93.2771607);
		locLong.add("-93.2771607");
		locs.add(craig);
		locNames.add("Craig Hall");
		
		Location robertwplaster = blankLoc;
		robertwplaster.setLatitude(37.1996232);
		locLat.add("37.1996232");
		robertwplaster.setLongitude(-93.2804042);
		locLong.add("-93.2804042");
		locs.add(robertwplaster);
		locNames.add("Robert W. Plaster Annex");
		
		Location athleticscenter = blankLoc;
		athleticscenter.setLatitude(37.1996232);
		locLat.add("37.1996232");
		athleticscenter.setLongitude(-93.2804042);
		locLong.add("-93.2804042");
		locs.add(athleticscenter);
		locNames.add("Athletics Center");
		
		Location kingstreetannex = blankLoc;
		kingstreetannex.setLatitude(37.1998616);
		locLat.add("37.1998616");
		kingstreetannex.setLongitude(-93.2803516);
		locLong.add("-93.2803516");
		locs.add(kingstreetannex);
		locNames.add("King Street Annex");
		
		Location reccenter = blankLoc;
		reccenter.setLatitude(37.2008477);
		locLat.add("37.2008477");
		reccenter.setLongitude(-93.2810454);
		locLong.add("-93.2810454");
		locs.add(reccenter);
		locNames.add("Recreational Center");
		
		Location jqh = blankLoc;
		jqh.setLatitude(37.2011307);
		locLat.add("37.2011307");
		jqh.setLongitude(-93.2811501);
		locLong.add("-93.2811501");
		locs.add(jqh);
		locNames.add("John Q. Hammons");
		
		Location performingarts = blankLoc;
		performingarts.setLatitude(37.2042504);
		locLat.add("37.2042504");
		performingarts.setLongitude(-93.2825926);
		locLong.add("-93.2825926");
		locs.add(performingarts);
		locNames.add("Perfomorming Arts");
		
		Location bearparknorth = blankLoc;
		bearparknorth.setLatitude(37.2041703);
		locLat.add("37.2041703");
		bearparknorth.setLongitude(-93.2825285);
		locLong.add("-93.2825285");
		locs.add(bearparknorth);
		locNames.add("Bear Park North");
		
		Location bearparksouth = blankLoc;
		bearparksouth.setLatitude(37.1980914);
		locLat.add("37.1980914");
		bearparksouth.setLongitude(-93.2838027);
		locLong.add("-93.2838027");
		locs.add(bearparksouth);
		locNames.add("Bear Park South");
		
		this.getLocation();
		
	}
	
	public void locs(View view){
		Intent s = new Intent(this, Locations.class);
        this.startActivity(s);
	}
	
	public void showNorth(View view){
		Context context = getApplicationContext();
		String text = "Now showing North";
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
		noNorth = false;
		TextView tv = (TextView) findViewById(R.id.locTitle);
		tv.setText("Current Heading: North");
	}
	
	public void showNorth2(){
		noNorth = false;
		TextView tv = (TextView) findViewById(R.id.locTitle);
		tv.setText("Current Heading: North");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gpstracker, menu);
		return true;
	}
	
	@Override
	protected void onResume() {

		sensorManager.registerListener(this, sensorAccelerometer,
				SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, sensorMagneticField,
				SensorManager.SENSOR_DELAY_NORMAL);
		super.onResume();
	}

	@Override
	protected void onPause() {

		sensorManager.unregisterListener(this, sensorAccelerometer);
		sensorManager.unregisterListener(this, sensorMagneticField);
		super.onPause();
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		//  Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		//  Auto-generated method stub
		

		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			for (int i = 0; i < 3; i++) {
				valuesAccelerometer[i] = event.values[i];
			}
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			for (int i = 0; i < 3; i++) {
				valuesMagneticField[i] = event.values[i];
			}
			break;
		}

		boolean success = SensorManager.getRotationMatrix(matrixR, matrixI,
				valuesAccelerometer, valuesMagneticField);

		if (success) {
			
			Location tempLoc = locs.get(pos);
			double tempLat = Double.parseDouble(locLat.get(pos));
			double tempLong = Double.parseDouble(locLong.get(pos));
			tempLoc.setLatitude(tempLat);
			tempLoc.setLongitude(tempLong);
			bearing = currentLocation.bearingTo(tempLoc);
			double tempBearing = bearing;
//			Log.e("bearing",tempBearing+"");
			
			SensorManager.getOrientation(matrixR, matrixValues);
			geoField = new GeomagneticField(
			         Double.valueOf(currentLocation.getLatitude()).floatValue(),
			         Double.valueOf(currentLocation.getLongitude()).floatValue(),
			         Double.valueOf(currentLocation.getAltitude()).floatValue(),
			         System.currentTimeMillis()
			      );
			
			float azimuth = matrixValues[0];
			double tempAz = (Math.toDegrees(azimuth) + 360)%360;
			double direction = matrixValues[0];
//			direction -= geoField.getDeclination();
//			double pitch = Math.toDegrees(matrixValues[1]);
//			double roll = Math.toDegrees(matrixValues[2]);
//			if (noNorth) {
////				azimuth = (float) Math.toDegrees(azimuth);
//				
////				if(azimuth < 0)
////					azimuth += 360;
//				
////				azimuth -= Math.toDegrees(geoField.getDeclination());
//				azimuth -= geoField.getDeclination();
//				
//				if (tempBearing < 0)
//					tempBearing += 360;
////				tempBearing = (float) Math.toRadians(tempBearing);
//				
////				Log.e("bearing",tempBearing+"");
//				
////				direction = azimuth - tempBearing;
//				direction = azimuth + tempBearing;
////				direction = tempBearing - azimuth;
////				
////				if (direction < 0)
////					direction += 360;
//				
////				direction = (float) Math.toRadians(direction);
//			}
//			if (noNorth) {
//				
//				azimuth -= Math.toDegrees(geoField.getDeclination());
//				
//				if (tempBearing < 0)
//					tempBearing += 360;
//
//				direction = tempAz + tempBearing;
//
//				direction = (float) Math.toRadians(direction);
//			}
			if (noNorth) {

				tempAz -= Math.toDegrees(geoField.getDeclination());

				if (tempBearing < 0)
					tempBearing += 360;

				direction = tempAz + tempBearing;

				direction = (float) Math.toRadians(direction);
			}
			myCompass.update(direction);
		}

	}
	
	public void getLocation(){
		noNorth = true;
		GPSUpdater mGPS = new GPSUpdater(this);
		
		currentLocation = new Location("Google");

		if (mGPS.canGetLocation && pos < locNames.size()) {

			double mLat = mGPS.getLatitude();
			double mLong = mGPS.getLongitude();

			currentLocation.setLatitude(mLat);
			currentLocation.setLongitude(mLong);
			Location tempLoc = locs.get(pos);
			double tempLat = Double.parseDouble(locLat.get(pos));
			double tempLong = Double.parseDouble(locLong.get(pos));
			tempLoc.setLatitude(tempLat);
			tempLoc.setLongitude(tempLong);

			float tempBearing = currentLocation.bearingTo(tempLoc);

			bearing = tempBearing;
//			Context context = getApplicationContext();
//			String temp = tempLoc.getLatitude()+" "+tempLoc.getLongitude()+" "+tempLoc.getProvider()+" "+pos;
//			int duration = Toast.LENGTH_SHORT;
//			Toast toast = Toast.makeText(context, temp, duration);
//			toast.show();

			String place = locNames.get(pos);

			TextView tv = (TextView) findViewById(R.id.locTitle);
			tv.setText("Current Heading: " + place);

		} else {
			Context context = getApplicationContext();
			String place = "Could not access your location. Please check that your GPS is enabled.";
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, place, duration);
			toast.show();
			this.showNorth2();
		}
	}

}
