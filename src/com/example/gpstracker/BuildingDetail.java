package com.example.gpstracker;

import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BuildingDetail extends Activity {
	
	SharedPreferences prefs;
	
	int pos;
		
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buildingdetail);
		
		prefs = this.getSharedPreferences(
			      "com.example.gpstracker.locations", Context.MODE_PRIVATE);
		pos = prefs.getInt("pos", 9999);

		DataAdapter myDbHelper = new DataAdapter(this);
		myDbHelper.createDatabase();      
		myDbHelper.open();
		
	 	ArrayList<Building> dbValues = myDbHelper.getAll();
	 		 	
	 	myDbHelper.close();		
						
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(dbValues.get(pos).name);
		TextView descr = (TextView) findViewById(R.id.buildingDescription);
		descr.setText(dbValues.get(pos).descr);
		
		new DownloadImageTask((ImageView) findViewById(R.id.buildingPic))
        .execute(dbValues.get(pos).pic_url);
		
	}
	
	public void goMap(View view){
		Intent s = new Intent(this, Map.class);
        startActivityForResult(s, 0);
	}
	
	public void locs(View view){
		Intent s = new Intent(this, Locations.class);
        this.startActivity(s);
	}
	
	public void showHeading(View view){
		Intent s = new Intent(view.getContext(), GPSTracker.class);
        startActivityForResult(s, 0);
	}

}
