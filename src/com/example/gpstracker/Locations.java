package com.example.gpstracker;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

public class Locations extends ListActivity {
	
	SharedPreferences prefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setListAdapter(ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.locList, R.layout.activity_listloc));
		
		prefs = this.getSharedPreferences(
			      "com.example.gpstracker.locations", Context.MODE_PRIVATE);
		
		getListView().setOnItemClickListener(new OnItemClickListener() {			
			@Override
		    public void onItemClick(AdapterView<?> parent, View view,
		            int position, long id) {
				prefs.edit().putInt("pos", position).commit();
				
				Intent s = new Intent(view.getContext(), BuildingDetail.class);
		        startActivityForResult(s, 0);
		    }
		});
	}
}
