package com.example.gpstracker;
 
public class Building {
 
    int id;
    String name;
    String descr;
    String pic_url;
    float lat;
    float longitude;
    
    // constructors
    public Building() {
    }
 
    public Building(int id, String name, String descr, String pic_url, float lat, float longitude) {
    	this.id = id;
    	this.name = name;
        this.descr = descr;
        this.pic_url = pic_url;
        this.lat = lat;
        this.longitude = longitude;
    }

    public Building(String name, String descr, String pic_url, float lat, float longitude) {
        this.name = name;
        this.descr = descr;
        this.pic_url = pic_url;
        this.lat = lat;
        this.longitude = longitude;
    }
 
    
    // setters
    public void setId(int id) {
        this.id = id;
    }
 
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDescr(String descr) {
    	this.descr = descr;
    }
 
    public void setPicUrl(String pic_url) {
    	this.pic_url = pic_url;
    }
    
    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
    
    // getters
    public long getId() {
        return this.id;
    }
 
    public String getName() {
        return this.name;
    }

    public String getDescr() {
        return this.descr;
    }

    public String getPicUrl() {
        return this.pic_url;
    }
    
    public float getLat() {
    	return this.lat;
    }
    
    public float getLongitude() {
        return this.longitude;
    }
}