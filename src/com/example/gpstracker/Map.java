package com.example.gpstracker;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Map extends FragmentActivity {
	
	SharedPreferences prefs;

	int pos;
	
	private LatLng cur;
	private LatLng dest;
	private GoogleMap map;
	private Location currentLocation;
	
	GPSUpdater mGPS;
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

		DataAdapter myDbHelper = new DataAdapter(this);
		myDbHelper.createDatabase();      
		myDbHelper.open();
		
	 	ArrayList<Building> dbValues = myDbHelper.getAll();
	 		 	
	 	myDbHelper.close();		

		
		prefs = this.getSharedPreferences("com.example.gpstracker.locations",
				Context.MODE_PRIVATE);
		pos = prefs.getInt("pos", 9999);
		
		currentLocation = new Location("Google");

		// initialize location objects

		mGPS = new GPSUpdater(this);

		currentLocation = new Location("Google");

		if (mGPS.canGetLocation) {

			double mLat = mGPS.getLatitude();
			double mLong = mGPS.getLongitude();

			currentLocation.setLatitude(mLat);
			currentLocation.setLongitude(mLong);

		} else {
			Context context = getApplicationContext();
			String place = "Could not access your location. Please check that your GPS is enabled.";
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, place, duration);
			toast.show();
		}

		cur = new LatLng(currentLocation.getLatitude(),
				currentLocation.getLongitude());
		dest = new LatLng(dbValues.get(pos).lat,
				dbValues.get(pos).longitude);
		
		map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

		map.addMarker(new MarkerOptions().position(cur)
				.title("Current Location"));
		Log.e("Current Location",currentLocation.getLatitude()+"");
		map.addMarker(new MarkerOptions()
				.position(dest)
				.title(dbValues.get(pos).name)
				.snippet("Missouri State")
				.icon(BitmapDescriptorFactory
			              .fromResource(R.drawable.building)));

		// Move the camera instantly to current location with a zoom of 15.
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(cur, 15));

		// Zoom in, animating the camera.
		map.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gpstracker, menu);
		return true;
	}
	
	public void buildingDetail(View view){
		Intent s = new Intent(this, BuildingDetail.class);
        this.startActivity(s);
	}
	
	public void locs(View view){
		Intent s = new Intent(this, Locations.class);
        this.startActivity(s);
	}
	
	public void showHeading(View view){
		Intent s = new Intent(view.getContext(), GPSTracker.class);
        startActivityForResult(s, 0);
	}

}
